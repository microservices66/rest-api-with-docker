package com.example.restapiwithdocker.service.impl;

import com.example.restapiwithdocker.mapper.CounterMapper;
import com.example.restapiwithdocker.model.dto.CounterDto;
import com.example.restapiwithdocker.model.entity.Counter;
import com.example.restapiwithdocker.repository.CounterRepository;
import com.example.restapiwithdocker.service.CounterService;
import org.springframework.stereotype.Service;

@Service
public class CounterServiceImpl implements CounterService {

    private final CounterRepository counterRepository;

    public CounterServiceImpl(CounterRepository counterRepository) {
        this.counterRepository = counterRepository;
    }

    @Override
    public Long getCounterValue() {
        Long counterValue = 0L;

        Counter counter = counterRepository.findFirstByOrderByIdDesc();
        CounterDto counterDto = CounterMapper.INSTANCE.entityToDto(counter);

        if (counterDto != null) {
            counterValue = counterDto.getCounterValue();
        }

        incrementCounterValue(counterValue);
        return counterValue;
    }

    private void incrementCounterValue(Long currentValue) {
        counterRepository.save(new Counter(currentValue + 1));
    }
}
