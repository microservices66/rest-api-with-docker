package com.example.restapiwithdocker;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RestApiWithDockerApplication {

    public static void main(String[] args) {
        SpringApplication.run(RestApiWithDockerApplication.class, args);
    }

}
