package com.example.restapiwithdocker.repository;

import com.example.restapiwithdocker.model.entity.Counter;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CounterRepository extends JpaRepository<Counter, Long> {
    Counter findFirstByOrderByIdDesc();
}
