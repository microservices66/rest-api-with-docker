package com.example.restapiwithdocker.controller;

import com.example.restapiwithdocker.service.CounterService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("hello")
@RestController
public class GreetingController {

    private final CounterService counterService;

    public GreetingController(CounterService counterService) {
        this.counterService = counterService;
    }

    @GetMapping
    public ResponseEntity<String> greetUser() {
        Long greetingCount = counterService.getCounterValue();
        String message = "Hello, the counter is " + greetingCount;
        return ResponseEntity.ok(message);
    }
}
