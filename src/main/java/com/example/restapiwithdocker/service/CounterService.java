package com.example.restapiwithdocker.service;

public interface CounterService {
    Long getCounterValue();
}
