package com.example.restapiwithdocker.mapper;

import com.example.restapiwithdocker.model.dto.CounterDto;
import com.example.restapiwithdocker.model.entity.Counter;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface CounterMapper {
    CounterMapper INSTANCE = Mappers.getMapper(CounterMapper.class);

    CounterDto entityToDto(Counter counter);
}
